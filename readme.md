Hello HTML24!

Here is my submission in response to the job test detailed below.

I have modeled the site through HTML, CSS, with an assist from Bootstrap 4, and a small amount of Javascript.

I'm very interested in finding an internship here in Copenhagen. I'm a Danish Resident and US Citizen and have been studying front end web development for several months now. 

Please contact me if you'd like to talk more or set up an interview.

Thanks & all the best,

Aram Shelton
+45 91 96 66 02

aramshelton@gmail.com



Brown Rabbit
============

This is the job test if you wish to be a web developer at HTML24

Contents
--------

When you have completed this test you will have demonstrated skills/knowledge in

* git
* html
* css
* js

Instructions
------------

To complete this test.

1. Fork this repository
2. Create a website based on the supplied PSD-file
3. Create a pull request to this repository