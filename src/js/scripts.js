// Script to autopopulate the sponsor logo images on page load.

$(document).ready(function () {
  
  for ( var i = 1; i <= 24; i++ ){
    $('#sponsor-logos').append(
    "<div class='sponsor-thumbnail-box'><img src='img/sponsor-" 
      + i + ".png' alt='' class='center-logo'></div>"
    );
  }

});